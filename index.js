const bodyParser = require("body-parser");                        //(1)
const express = require("express");                               //(2)
const fs = require("fs");                                         //(3)

const app = express();                                            //(4)

app.use(bodyParser.json());                                       //(5)
app.use(bodyParser.urlencoded({extended: true}));                 //(6)


const getData = (path) => {
    let json = fs.readFileSync(path);                             //(8)(11)

    return JSON.parse(json);                                      //(9)(12)
}

const f1 = getData('./db/user.json')                              //(7)
const f2 = getData('./db/photo.json')                             //(10)

setTimeout(myFunction, 1000)                                      //(14)

Promise.all([f1, f2]).then(([res1, res2]) => {                    //(11)
    console.log('Results', res1, res2)                            //(12)
})
    .catch(err => {
        console.error(err)                                        //(13)
    })



const myFunction = () => {
    setTimeout(myFunction, 1000)                                  //(15)
}

